terraform {
  backend "gcs" {
    # Creds JSON must be in environment variable GOOGLE_CREDENTIALS=<path.json>
    bucket  = "k8s-hack-alto-thon-gcp-remote-state"
    prefix  = "terraform/state"
  }
}